#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#define MAX 5

typedef struct Prop {
	int id;
	char* cont;
	int cs,cf,cb,co;
}Prop;

typedef struct _Noeud {
	Prop val;
	struct _Noeud *fg, *fd;
} NOEUD;

typedef NOEUD * ARBRE ;

Prop pro1;
Prop pro2;
Prop pro3;
Prop pro4;
Prop pro5;
Prop pro6;
Prop pro7;
Prop pro8;
Prop pro9;
Prop pro10;
Prop pro11;
Prop pro12;
Prop pro13;
Prop pro14;
Prop pro15;
Prop pro16;
Prop pro17;
Prop pro18;
Prop pro19;
Prop pro20;
Prop pro21;
Prop pro22;
Prop pro23;
Prop pro24;
Prop pro25;
Prop pro26;
Prop pro27;
Prop pro28;
Prop pro29;
Prop pro30;
Prop pro31;
Prop pro32;
Prop pro33;
Prop pro34;
Prop pro35;
Prop pro36;
Prop pro37;
Prop pro38;
Prop pro39;
Prop pro40;
Prop pro41;
Prop pro42;
Prop pro43;
Prop pro44;
Prop pro45;
Prop pro46;
Prop pro47;
Prop pro48;
Prop pro49;
Prop pro50;
Prop pro51;
Prop pro52;
Prop pro53;
Prop ques[MAX];
Prop result;

ARBRE Arbre_sasiete = NULL;
ARBRE Arbre_foi = NULL;
ARBRE Arbre_bonheur = NULL;
ARBRE Arbre_ordre = NULL;
ARBRE Arbre_prop = NULL;

FILE *menu = NULL;
FILE *sasiete = NULL;
FILE *foi = NULL;
FILE *bonheur = NULL;
FILE *ordre = NULL;
FILE *Fin=NULL;

int compteur_sasiete=5;
int compteur_foi=5;
int compteur_bonheur=5;
int compteur_ordre=5;
int jour=1;
int fin=0;
int aleat;

char* ok;

Prop creerProp(int id, char* cont, int cs, int cf, int cb, int co){
    Prop n;
    n.id = id;
    n.cont = cont;
    n.cs = cs;
    n.cf = cf;
    n.cb = cb;
    n.co = co;
    return n;
}

void affProp(Prop p){
    printf("id: %d\n",p.id);
    printf("cont: %s\n",p.cont);
    printf("cs: %d\n",p.cs);
    printf("cf: %d\n",p.cf);
    printf("cb: %d\n",p.cb);
    printf("co: %d\n",p.co);
}

ARBRE creerNoeud(Prop nval) {
	ARBRE nouveau = (ARBRE) malloc(sizeof(NOEUD));
	if(nouveau){
		nouveau->val = nval;
		nouveau->fg = nouveau->fd = NULL;
	}
	return nouveau;
}

ARBRE inserer(Prop nval, ARBRE racine){
	if(racine==NULL){
		return creerNoeud(nval);
	}
	else{
		if(nval.id <= racine->val.id){
			racine->fg = inserer(nval, racine->fg);
		}
		else{
			racine->fd = inserer(nval, racine->fd);
		}
		return racine;
	}	
}

void creerArbre(){
    pro1 = creerProp(5,"JAUGE/JAUGE SASIETE/SASIETE5.txt",0,0,0,0);
    pro2 = creerProp(3,"JAUGE/JAUGE SASIETE/SASIETE3.txt",0,0,0,0);
    pro3 = creerProp(1,"JAUGE/JAUGE SASIETE/SASIETE1.txt",0,0,0,0);
    pro4 = creerProp(4,"JAUGE/JAUGE SASIETE/SASIETE4.txt",0,0,0,0);
    pro5 = creerProp(2,"JAUGE/JAUGE SASIETE/SASIETE2.txt",0,0,0,0);
    pro6 = creerProp(-1,"JAUGE/JAUGE SASIETE/FIN_SASIETE0.txt",0,0,0,0);
    pro7 = creerProp(7,"JAUGE/JAUGE SASIETE/SASIETE7.txt",0,0,0,0);
    pro8 = creerProp(9,"JAUGE/JAUGE SASIETE/SASIETE9.txt",0,0,0,0);
    pro9 = creerProp(6,"JAUGE/JAUGE SASIETE/SASIETE6.txt",0,0,0,0);
    pro10 = creerProp(8,"JAUGE/JAUGE SASIETE/SASIETE8.txt",0,0,0,0);
    pro11 = creerProp(10,"JAUGE/JAUGE SASIETE/FIN_SASIETE1.txt",0,0,0,0);
    pro12 = creerProp(0,"JAUGE/JAUGE SASIETE/SASIETE0.txt",0,0,0,0);

    Arbre_sasiete = inserer(pro1,Arbre_sasiete);
    Arbre_sasiete = inserer(pro2,Arbre_sasiete);
    Arbre_sasiete = inserer(pro3,Arbre_sasiete);
    Arbre_sasiete = inserer(pro4,Arbre_sasiete);
    Arbre_sasiete = inserer(pro5,Arbre_sasiete);
    Arbre_sasiete = inserer(pro6,Arbre_sasiete);
    Arbre_sasiete = inserer(pro7,Arbre_sasiete);
    Arbre_sasiete = inserer(pro8,Arbre_sasiete);
    Arbre_sasiete = inserer(pro9,Arbre_sasiete);
    Arbre_sasiete = inserer(pro10,Arbre_sasiete);
    Arbre_sasiete = inserer(pro11,Arbre_sasiete);
    Arbre_sasiete = inserer(pro12,Arbre_sasiete);

    pro13 = creerProp(5,"JAUGE/JAUGE FOI/FOI5.txt",0,0,0,0);
    pro14 = creerProp(3,"JAUGE/JAUGE FOI/FOI3.txt",0,0,0,0);
    pro15 = creerProp(1,"JAUGE/JAUGE FOI/FOI1.txt",0,0,0,0);
    pro16 = creerProp(4,"JAUGE/JAUGE FOI/FOI4.txt",0,0,0,0);
    pro17 = creerProp(2,"JAUGE/JAUGE FOI/FOI2.txt",0,0,0,0);
    pro18 = creerProp(-1,"JAUGE/JAUGE FOI/FIN_FOI0.txt",0,0,0,0);
    pro19 = creerProp(7,"JAUGE/JAUGE FOI/FOI7.txt",0,0,0,0);
    pro20 = creerProp(9,"JAUGE/JAUGE FOI/FOI9.txt",0,0,0,0);
    pro21 = creerProp(6,"JAUGE/JAUGE FOI/FOI6.txt",0,0,0,0);
    pro22 = creerProp(8,"JAUGE/JAUGE FOI/FOI8.txt",0,0,0,0);
    pro23 = creerProp(10,"JAUGE/JAUGE FOI/FIN_FOI1.txt",0,0,0,0);
    pro24 = creerProp(0,"JAUGE/JAUGE FOI/FOI0.txt",0,0,0,0);

    Arbre_foi = inserer(pro13,Arbre_foi);
    Arbre_foi = inserer(pro14,Arbre_foi);
    Arbre_foi = inserer(pro15,Arbre_foi);
    Arbre_foi = inserer(pro16,Arbre_foi);
    Arbre_foi = inserer(pro17,Arbre_foi);
    Arbre_foi = inserer(pro18,Arbre_foi);
    Arbre_foi = inserer(pro19,Arbre_foi);
    Arbre_foi = inserer(pro20,Arbre_foi);
    Arbre_foi = inserer(pro21,Arbre_foi);
    Arbre_foi = inserer(pro22,Arbre_foi);
    Arbre_foi = inserer(pro23,Arbre_foi);
    Arbre_foi = inserer(pro24,Arbre_foi);

    pro25 = creerProp(5,"JAUGE/JAUGE BONHEUR/BONHEUR5.txt",0,0,0,0);
    pro26 = creerProp(3,"JAUGE/JAUGE BONHEUR/BONHEUR3.txt",0,0,0,0);
    pro27 = creerProp(1,"JAUGE/JAUGE BONHEUR/BONHEUR1.txt",0,0,0,0);
    pro28 = creerProp(4,"JAUGE/JAUGE BONHEUR/BONHEUR4.txt",0,0,0,0);
    pro29 = creerProp(2,"JAUGE/JAUGE BONHEUR/BONHEUR2.txt",0,0,0,0);
    pro30 = creerProp(-1,"JAUGE/JAUGE BONHEUR/FIN_BONHEUR0.txt",0,0,0,0);
    pro31 = creerProp(7,"JAUGE/JAUGE BONHEUR/BONHEUR7.txt",0,0,0,0);
    pro32 = creerProp(9,"JAUGE/JAUGE BONHEUR/BONHEUR9.txt",0,0,0,0);
    pro33 = creerProp(6,"JAUGE/JAUGE BONHEUR/BONHEUR6.txt",0,0,0,0);
    pro34 = creerProp(8,"JAUGE/JAUGE BONHEUR/BONHEUR8.txt",0,0,0,0);
    pro35 = creerProp(10,"JAUGE/JAUGE BONHEUR/FIN_BONHEUR1.txt",0,0,0,0);
    pro36 = creerProp(0,"JAUGE/JAUGE BONHEUR/BONHEUR0.txt",0,0,0,0);

    Arbre_bonheur = inserer(pro25,Arbre_bonheur);
    Arbre_bonheur = inserer(pro26,Arbre_bonheur);
    Arbre_bonheur = inserer(pro27,Arbre_bonheur);
    Arbre_bonheur = inserer(pro28,Arbre_bonheur);
    Arbre_bonheur = inserer(pro29,Arbre_bonheur);
    Arbre_bonheur = inserer(pro30,Arbre_bonheur);
    Arbre_bonheur = inserer(pro31,Arbre_bonheur);
    Arbre_bonheur = inserer(pro32,Arbre_bonheur);
    Arbre_bonheur = inserer(pro33,Arbre_bonheur);
    Arbre_bonheur = inserer(pro34,Arbre_bonheur);
    Arbre_bonheur = inserer(pro35,Arbre_bonheur);
    Arbre_bonheur = inserer(pro36,Arbre_bonheur);

    pro37 = creerProp(5,"JAUGE/JAUGE ORDRE/ORDRE5.txt",0,0,0,0);
    pro38 = creerProp(3,"JAUGE/JAUGE ORDRE/ORDRE3.txt",0,0,0,0);
    pro39 = creerProp(1,"JAUGE/JAUGE ORDRE/ORDRE1.txt",0,0,0,0);
    pro40 = creerProp(4,"JAUGE/JAUGE ORDRE/ORDRE4.txt",0,0,0,0);
    pro41 = creerProp(2,"JAUGE/JAUGE ORDRE/ORDRE2.txt",0,0,0,0);
    pro42 = creerProp(-1,"JAUGE/JAUGE ORDRE/FIN_ORDRE0.txt",0,0,0,0);
    pro43 = creerProp(7,"JAUGE/JAUGE ORDRE/ORDRE7.txt",0,0,0,0);
    pro44 = creerProp(9,"JAUGE/JAUGE ORDRE/ORDRE9.txt",0,0,0,0);
    pro45 = creerProp(6,"JAUGE/JAUGE ORDRE/ORDRE6.txt",0,0,0,0);
    pro46 = creerProp(8,"JAUGE/JAUGE ORDRE/ORDRE8.txt",0,0,0,0);
    pro47 = creerProp(10,"JAUGE/JAUGE ORDRE/FIN_ORDRE1.txt",0,0,0,0);
    pro48 = creerProp(0,"JAUGE/JAUGE ORDRE/ORDRE0.txt",0,0,0,0);

    Arbre_ordre = inserer(pro37,Arbre_ordre);
    Arbre_ordre = inserer(pro38,Arbre_ordre);
    Arbre_ordre = inserer(pro39,Arbre_ordre);
    Arbre_ordre = inserer(pro40,Arbre_ordre);
    Arbre_ordre = inserer(pro41,Arbre_ordre);
    Arbre_ordre = inserer(pro42,Arbre_ordre);
    Arbre_ordre = inserer(pro43,Arbre_ordre);
    Arbre_ordre = inserer(pro44,Arbre_ordre);
    Arbre_ordre = inserer(pro45,Arbre_ordre);
    Arbre_ordre = inserer(pro46,Arbre_ordre);
    Arbre_ordre = inserer(pro47,Arbre_ordre);
    Arbre_ordre = inserer(pro48,Arbre_ordre);

    pro49 = creerProp(2,"Les paroissiens organise une procession nocturne, voulez-vous les accompagnez ?",0,2,1,0);
    pro50 = creerProp(0,"Le Royaume voisin est entré en guerre contre le Royaume d'Angleterre et demande votre aide, voulez-vous l'aider ?",-1,1,-1,2);
    //pro51 = creerProp(1,"test1",1,1,1,1);
    //pro52 = creerProp(3,"test2",0,0,0,0);
   // pro53 = creerProp(4,"test3",2,2,2,2);

    ques[0]=pro50;
    //ques[1]=pro51;
    ques[2]=pro49;
    //ques[3]=pro52;
    //ques[4]=pro53;
}

char* rechercheArbre(int r, ARBRE a){
    if(a != NULL){
        if(r < a->val.id){
            rechercheArbre(r,a->fg);
        }
        else{
            if(r > a->val.id){
                rechercheArbre(r,a->fd);
            }
            else{
                return a->val.cont;
                
            }
        }
    }
}

void suppMax(ARBRE a){
    if(a->fd == NULL){
        printf("%d\n",a->val.id);
        a = a->fg;
    }
    else{
        suppMax(a->fd);
    }
}

void suppArbre(Prop p, ARBRE a){
    if(a != NULL){
        if(p.id < a->val.id){
            suppArbre(p,a->fg);
        }
        else{
            if(p.id > a->val.id){
                suppArbre(p,a->fd);
            }
            else{
                if(a->fg == NULL){
                    a = a->fd;
                }
                else{
                    if(a->fd == NULL){
                        a = a->fg;
                    }
                    else{
                        suppMax(a->fg);
                    }
                }
            }
        }
    }
}

void afficher(ARBRE a) {
	if (a){
		afficher(a->fg);
		printf("%d\n", a->val.id);
		afficher(a->fd);
	}
}

void affCompt(int compt, FILE* f, ARBRE a){
    char* o;
    if(compt != -2){
        o = rechercheArbre(compt,a);
        f=fopen(o, "r");
        if (f != NULL){
            char c=fgetc(f);
            while(c != EOF){
        	    printf("%c",c);
        	    c=fgetc(f);
		    }
            printf("\n\n");
            fclose(f);
        }
    }
    
}

int aleatoire(){
    int alea;
    srand(time(NULL));
    alea=rand()%5;
    return alea;
}  
